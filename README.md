# Questionnaire exercise

### Installation
After cloning the repository locally, run ```npm install``` to install all the dependencies of the project. Locally, I ran the project under node v8.6.0 (I have not tested it with older versions).

### Start or build the application
Run ```npm start``` to start the application in development mode, or ```npm run build``` to create the production assets folder (in build directory) and then you can run something like ```python -m SimpleHTTPServer``` from within the build directory in order to start a server on http://localhost:8000.

### Tests
I have created a few indicative tests (not an exhaustive test suite) for different test categories (unit and integration). In this context, I've created a unit test for ```utils.js```, for ```AnswerLimit.js``` component, as well as integration tests for ```Store.js``` and ```Survey.js```, in order to showcase use of jest, enzyme and sinon for mocking. In order to run tests, run ```npm test```.

### Notes and assumptions

##### Schema
The schema of the survey is shown below (with sample data):
```javascript
{
    "questions": [
        {
            "id": 1,
            "prompt": "question text",
            "settings": {
                "limit": {
                    "min": 1,
                    "max": 3
                }
            },
            "answers": [
                {
                    "id": 1,
                    "text": "answer 1 text",
                },
                {
                    "id": 2,
                    "text": "answer 2 text",
                }
            ]
        }
    ]
}
```

Id is an identifier of questions or answers that is unique (auto incrementing) in the scope of each entity (question or answer). The order of questions and answers is assumed to be the order as displayed in the json (i.e. the index of the question or answer). If database was a concern (i.e. in real-world scenarios) probably order should be an attribute of the records.

##### Data persistence
By data persistence I mean the persistence to the state object that holds the survey structure and not some database.
Here are the cases when data are saved:

* Every time "Add new question" button is clicked, an empty question is created (with a valid id and empty settings and answers).
* On change of the value of the question prompt
* On addition of a new answer
* On change of the limit answer fields (checkbox or any of the limits)
* When questions or answers are reordered or deleted

##### State management
For state management, I haven't added a dependency in the project. In general I would use one of the tried and tested state management tools & methods (e.g. redux, reflux, flux, mobx etc).
For this exercise, I've created (in ```Store.js```) a "poor man's" redux/flux hybrid, where an action and data is dispatched and a reducer produces the next state. The state is stored and can be used by any component (much like redux). Optionally, dispatching an action accepts a callback (like flux implementations) that can be used to change components local state (for re-renders etc).
State object is an immutable Map and all of state transformations are done using immutable objects and methods.

##### Data validation
In general, validation has been kept to a minimum. I've sticked to the spec document and have added validation only for the answer limiting functionality. In real-world applications I'd probably use a validation library like validate.js (or build validation methods). Also, apart from front-end validation, validation on the server should be in place, too. Since validation is incomplete, the state might include invalid data (like questions without answers or with empty prompt).
The validation of the answer limiting functionality is shown with validation state classes (fields turn red). The rules used are:

* minimum answers should be a number, non-negative and not greater than the number of answers
* maximum answers can be either empty, or a non-negative number not greater than the number of answers
* maximum answers cannot be less than minimum answers
* the above rules apply only when limiting is enabled
