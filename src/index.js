import React from 'react';
import ReactDOM from 'react-dom';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import './index.css';

import Survey from './components/Survey';

ReactDOM.render(<Survey />, document.getElementById('root'));
