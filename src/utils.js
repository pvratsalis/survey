/*
 * Given an immutable list and the property for which we want a new value,
 * it returns the next value
 *
 * Example:
 *
 * > nextValue({ list: List([Map({ id: 1 }), Map({ id: 2 })]), property: 'id', defaultValue: 1 })
 * 3
 */
export function nextValue({ list, property, defaultValue }) {
    return list.size === 0 ? defaultValue : list.maxBy(item => item.get(property)).get(property) + 1;
}