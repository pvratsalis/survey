import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { state } from '../Store';
import EntryWithControls from './EntryWithControls';
import AnswerText from './AnswerText';
import AnswerLimit from './AnswerLimit';

export default class Question extends Component {
    static propTypes = {
        question: PropTypes.object.isRequired,
        questionPromptPlaceholder: PropTypes.string,
        afterRemoveOrReorder: PropTypes.func.isRequired,
        number: PropTypes.number.isRequired
    };

    static defaultProps = {
        questionPromptPlaceholder: "Enter your question prompt..."
    };

    constructor(props) {
        super(props);
        this.state = { question: this.props.question };
      }

    change(state) {
        let question = state.get('questions').filter(q => q.get('id') === this.props.question.get('id')).get(0);
        this.setState({ question: question });
    }

    _renderAnswersSection() {
        let question = this.state.question;
        let answers = question.get('answers');

        if (answers.size > 0) {
            return (
                <div className="mt-medium">
                    <h3>Answers</h3>
                    {this._renderAnswers()}
                </div>
            );
        }
    }

    _renderAnswers() {
        let question = this.state.question;
        let answers = question.get('answers');

        return answers.map((item) => {
            return <EntryWithControls key={`${question.get('id')}-${item.get('id')}`} type="answer" item={item}
                afterRemoveOrReorder={this.change.bind(this)} value={item.get('text')}
                placeholder={this.props.questionPromptPlaceholder}
                parent={this.state.question}
                first={this.state.question.getIn(['answers', 0, 'id']) === item.get('id')}
                readOnly={true} last={this.state.question.getIn(['answers', -1, 'id']) === item.get('id')} />
        });
    }

    render() {
        return (
            <div className="question-card">
                <h3>Question {this.props.number}</h3>
                <EntryWithControls type="question" item={this.state.question}
                    afterRemoveOrReorder={this.props.afterRemoveOrReorder.bind(this)}
                    first={state.getIn(['questions', 0, 'id']) === this.props.question.get('id')}
                    readOnly={false} last={state.getIn(['questions', -1, 'id']) === this.state.question.get('id')} />
                <AnswerLimit question={this.state.question} />
                {this._renderAnswersSection()}
                <div className="row mt-medium">
                    <div className="col-xs-12 col-sm-6">
                        <AnswerText onSave={this.change.bind(this)} question={this.state.question} />
                    </div>
                </div>
            </div>
        );
    }
}