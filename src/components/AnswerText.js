import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { FormControl } from 'react-bootstrap';
import { dispatch } from '../Store';

export default class AnswerText extends Component {
    static propTypes = {
        question: PropTypes.object.isRequired,
        onSave: PropTypes.func.isRequired,
        placeholder: PropTypes.string,
        value: PropTypes.string
    };

    static defaultProps = {
        placeholder: 'add answer here...',
        value: ''
    };

    constructor(props) {
        super(props);
        this.state = { value: this.props.value }
    }

    onChange(e) {
        this.setState({ value: e.target.value });
    }

    createAnswer(e) {
        if (e.key === 'Enter') {
            dispatch('CREATE_ANSWER', { questionId: this.props.question.get('id'), value: e.target.value }, this.props.onSave)
            this.setState({ value: '' });
        }
    }

    render() {
        return (
            <FormControl placeholder={this.props.placeholder} type="text" value={this.state.value} onKeyPress={this.createAnswer.bind(this)} onChange={this.onChange.bind(this)} />
        );
    }
}