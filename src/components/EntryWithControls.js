import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { Button, FormControl } from 'react-bootstrap';
import { dispatch } from '../Store';

export default class EntryWithControls extends Component {
    static propTypes = {
        readOnly: PropTypes.bool.isRequired,
        type: PropTypes.string.isRequired,
        afterRemoveOrReorder: PropTypes.func.isRequired,
        item: PropTypes.object.isRequired,
        first: PropTypes.bool.isRequired,
        last: PropTypes.bool.isRequired,
        placeholder: PropTypes.string,
        value: PropTypes.string,
        parent: PropTypes.object
    };

    static defaultProps = {
        placeholder: '',
        onChange: () => {},
        parent: null
    };

    remove() {
        let action = this.props.type === 'question' ? 'REMOVE_QUESTION' : 'REMOVE_ANSWER';
        let data = this.props.type === 'question' ? { id: this.props.item.get('id') } : { questionId: this.props.parent.get('id'), id: this.props.item.get('id') }
        dispatch(action, data, this.props.afterRemoveOrReorder);
    }

    reorder(direction) {
        if (direction === 'up' && this.props.first) {
            return;
        }

        if (direction === 'down' && this.props.last) {
            return;
        }

        let action = this.props.type === 'question' ? 'REORDER_QUESTION' : 'REORDER_ANSWER';
        let data = this.props.type === 'question' ? { id: this.props.item.get('id') } : { questionId: this.props.parent.get('id'), id: this.props.item.get('id') }
        dispatch(action, Object.assign(data, { direction: direction }), this.props.afterRemoveOrReorder);
    }

    updateQuestionPrompt(e) {
        dispatch('UPDATE_QUESTION_PROMPT', { id: this.props.item.get('id'), value: e.target.value });
    }

    render() {
        return (
            <div className="row">
                <div className="col-xs-6 col-sm-8">
                    <FormControl disabled={this.props.readOnly} value={this.props.value}
                        placeholder={this.props.placeholder} type="string" onChange={this.updateQuestionPrompt.bind(this)} />
                </div>
                <div className="col-xs-6 col-sm-4">
                    <Button disabled={this.props.first} onClick={this.reorder.bind(this, 'up')}><i className="glyphicon glyphicon-chevron-up"></i></Button>
                    <Button disabled={this.props.last} onClick={this.reorder.bind(this, 'down')}><i className="glyphicon glyphicon-chevron-down"></i></Button>
                    <Button onClick={this.remove.bind(this)}><i className="glyphicon glyphicon-trash"></i></Button>
                </div>
            </div>
        );
    }
}