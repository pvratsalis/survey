import React, { Component } from 'react';
import { Button, Alert } from 'react-bootstrap';
import Question from '../Question';
import { dispatch, state } from '../../Store';

import './index.css';

export default class Survey extends Component {
  constructor(props) {
    super(props);
    this.state = { lastStructureUpdate: new Date() };
  }

  change(state) {
    this.setState({ lastStructureUpdate: new Date() });
  }

  createQuestion() {
    dispatch('CREATE_QUESTION', {}, this.change.bind(this));
  }

  _renderQuestions() {
    return state.get('questions').map((q, idx) => {
        return <Question key={q.get('id')} number={idx+1}
                         question={q} afterRemoveOrReorder={this.change.bind(this)} />;
    });
  }

  _renderEmptySurveyMessage() {
    if (state.get('questions').size === 0) {
      return <Alert bsStyle="warning">There are no questions in the questionnaire.</Alert>
    }
  }

  _renderAddQuestionButtonOrMessage() {
    if (state.get('questions').size === 10) {
      return (
        <div className="col-xs-12 col-sm-4 mt-small">
          <Alert bsStyle="info">Questions limit reached</Alert>
        </div>
      );
    } else {
      return (
        <div className="col-xs-12 col-sm-4">
          <Button onClick={this.createQuestion.bind(this)}
                  bsStyle="primary"
                  className="add-new-question pull-right">
            Add new question
          </Button>
        </div>
      );
    }
  }

  render() {
    return (
      <div className="container survey-container">
        <div className="row">
          <div className="col-xs-12 col-sm-8">
            <h1>Create your Questionnaire</h1>
          </div>
          {this._renderAddQuestionButtonOrMessage()}
        </div>
        {this._renderEmptySurveyMessage()}
        {this._renderQuestions()}
      </div>
    );
  }
}
