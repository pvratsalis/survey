import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Checkbox, FormControl, FormGroup, ControlLabel, Form } from 'react-bootstrap';
import { dispatch } from '../../Store';

import './index.css';

export default class AnswerLimit extends Component {
    static propTypes = {
        question: PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);
        this.state = { enabled: false, min: null, max: null };
    }

    componentDidUpdate(prevProps, prevState) {
        let settings = null;

        if (this.state.enabled && this.state.min) {
            settings = { limit: { min: +this.state.min, max: +this.state.max || null } };
        }

        dispatch('UPDATE_QUESTION_SETTINGS', { id: this.props.question.get('id'), settings: settings});
    }

    toggleEnabled(e) {
        this.setState({ enabled: !this.state.enabled, min: null, max: null });
    }

    updateLimit(type, e) {
        this.setState({ [type]: e.target.value || null });
    }

    getMinValidationState() {
        if (this.state.min <= 0 || isNaN(this.state.min)) {
            return 'error';
        }

        return +this.state.min > this.props.question.get('answers').size ? 'error' : null;
    }

    getMaxValidationState() {
        if (this.state.max === null) {
            return;
        }

        if (this.state.max <= 0 || isNaN(this.state.max)) {
            return 'error';
        }

        if (+this.state.max < +this.state.min) {
            return 'error';
        }

        return +this.state.max > this.props.question.get('answers').size ? 'error' : null;
    }

    _renderMin() {
        if (this.state.enabled) {
            return (
                <FormGroup validationState={this.getMinValidationState()}>
                    <ControlLabel>At least</ControlLabel>
                    <FormControl type="text" onChange={this.updateLimit.bind(this, 'min')} />
                </FormGroup>
            );
        }
    }

    _renderMax() {
        if (this.state.enabled) {
            return (
                <FormGroup validationState={this.getMaxValidationState()}>
                    <ControlLabel>No more than</ControlLabel>
                    <FormControl type="text" onChange={this.updateLimit.bind(this, 'max')} />
                </FormGroup>
            );
        }
    }

    render() {
        return (
            <Form inline={true} className="answer-limit">
                <FormGroup>
                    <Checkbox onChange={this.toggleEnabled.bind(this)}> Limit answers</Checkbox>
                </FormGroup>
                {this._renderMin()}
                {this._renderMax()}
            </Form>
        );
    }
}