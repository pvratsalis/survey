import { Map, List } from 'immutable';
import { nextValue } from './utils';

const initialState = Map({ questions: List() });

/*
 * state is an object that holds the state of the applications (the survey
 * contents for this exercise).
 */
let state = initialState;

/*
 * Question and Answer classes are just containers of static methods
 * that encapsulate the state transformations.
 */
class Question {
    static create(state, data) {
        let questions = state.get('questions');
        let nextId = nextValue({ list: questions, property: 'id', defaultValue: 1});
        return state.update('questions', q => q.push(Map({ id: nextId, settings: Map(), answers: List() })));
    }

    static remove(state, data) {
        return state.update('questions', q => q.filter(item => item.get('id') !== data.id))
    }

    static reorder(state, data) {
        let direction = data.direction === 'up' ? -1 : 1;
        let idx = state.get('questions').findIndex(item => item.get('id') === data.id);
        let question = state.get('questions').get(idx);
        return state.update('questions', q => q.delete(idx).insert(idx + direction, question));
    }

    static updatePrompt(state, data) {
        return state.update(
            'questions',
            q => q.map(item => item.get('id') === data.id ? item.set('prompt', data.value) : item )
        );
    }

    static updateSettings(state, data) {
        let idx = state.get('questions').findIndex(item => item.get('id') === data.id);
        return state.updateIn(['questions', idx, 'settings'], q => data.settings ? Map(data.settings) : Map());
    }
}

class Answer {
    static create(state, data) {
        let idx = state.get('questions').findIndex(item => item.get('id') === data.questionId);
        return state.updateIn(['questions', idx, 'answers'], q => q.push(
            Map({ id: nextValue({ list: q, property: 'id', defaultValue: 1 }), text: data.value })
        ));
    }

    static remove(state, data) {
        let questionIdx = state.get('questions').findIndex(item => item.get('id') === data.questionId);
        return state.updateIn(['questions', questionIdx, 'answers'], q => q.filter(item => item.get('id') !== data.id));
    }

    static reorder(state, data) {
        let direction = data.direction === 'up' ? -1 : 1;
        let questionIdx = state.get('questions').findIndex(item => item.get('id') === data.questionId);
        let answerIdx = state.getIn(['questions', questionIdx, 'answers']).findIndex(item => item.get('id') === data.id);
        let answer = state.getIn(['questions', questionIdx, 'answers', answerIdx]);
        return state.updateIn(['questions', questionIdx, 'answers'], q => q.delete(answerIdx).insert(answerIdx + direction, answer));
    }
}

/*
 * The reducer handles all the actions that trigger changes to the state.
 */
const reducer = function(state=initialState, action, data={}) {
    switch(action) {
        case 'CREATE_QUESTION':
            return Question.create(state, data);
        case 'UPDATE_QUESTION_PROMPT':
            return Question.updatePrompt(state, data);
        case 'UPDATE_QUESTION_SETTINGS':
            return Question.updateSettings(state, data);
        case 'REMOVE_QUESTION':
            return Question.remove(state, data);
        case 'REORDER_QUESTION':
            return Question.reorder(state, data);
        case 'CREATE_ANSWER':
            return Answer.create(state, data);
        case 'REMOVE_ANSWER':
            return Answer.remove(state, data);
        case 'REORDER_ANSWER':
            return Answer.reorder(state, data);
        default:
            return state;
    }
};

/*
 * The dispatch passes the action dispatched, along with some data
 * to the reducer. If a callback is provided, it is called with the state
 * as argument, in order to facilitate components state changes (flux-like).
 */
const dispatch = function(action, data={}, cb=() => {}) {
    state = reducer(state, action, data);
    cb(state);
};

export { state, dispatch };