import React from 'react';
import AnswerLimit from '../components/AnswerLimit';
import Enzyme, { shallow } from 'enzyme';
import { Map, List } from 'immutable';
import Adapter from 'enzyme-adapter-react-16';
import { Checkbox, FormControl, FormGroup } from 'react-bootstrap';

Enzyme.configure({ adapter: new Adapter() });

const sut = function() {
    let question = Map({ id: 1, settings: Map(), answers: List() });
    return shallow(<AnswerLimit question={question} />);
};

describe('<AnswerLimit />', () => {
    test('it starts no inputs', () => {
        const subject = sut();
        expect(subject.find(FormControl).length).toBe(0);
    });

    test('when checkbox is checked, it reveals minimum and maximum answers inputs', () => {
        const subject = sut();
        subject.find(Checkbox).simulate('change');
        expect(subject.find(FormControl).length).toBe(2);
    });

    test('inputs have error class with invalid validation', function() {
        const subject = sut();
        subject.find(Checkbox).simulate('change');
        subject.find(FormControl).last().simulate('change', { target: { value: '5' }});
        expect(subject.find(FormGroup).last().prop('validationState')).toBe('error');
    });
});