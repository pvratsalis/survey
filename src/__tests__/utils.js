import { nextValue } from '../utils';
import { List, Map } from 'immutable';

describe('nextvalue', () => {
    test('returns defaultValue on empty List', () => {
        expect(nextValue({ list: List(), property: 'id', defaultValue: 24 })).toBe(24);
    });

    test('returns next id on populated list', () => {
        let list = List([Map({ id: 4 })]);
        expect(nextValue({ list, property: 'id', defaultValue: 1 })).toBe(5);
    });
});
