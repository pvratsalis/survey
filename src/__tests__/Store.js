import { state, dispatch } from '../Store';
import sinon from 'sinon';

describe('dispatch', () => {
    test('updates state', () => {
        dispatch('CREATE_QUESTION', {});
        expect(state.get('questions').size).toBe(1);
    });

    test('with callback provided, calls the callback with state', () => {
        let cb = sinon.spy();
        dispatch('CREATE_QUESTION', {}, cb);
        expect(cb.calledOnce).toBe(true);
        expect(cb.calledWith(state)).toBe(true);
    });
});
