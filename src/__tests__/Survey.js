import ReactDOM from 'react-dom';
import React from 'react';
import Survey from '../components/Survey';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Survey />, div);
});